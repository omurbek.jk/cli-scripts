#!/bin/sh
# Requires jq and coreo cli
# Usage
# sh create_duplicate_plans.sh COREO_COMPOSITE_ID COREO_CLOUD_ID PLAN_NAME_PREFIX PLAN_CONFIG_SOURCE_JSON NUMBER_OF_PLANS [additional coreo CLI options]

[ $# -ge 5 ] || exit 1

composite_id=$1
cloud_id=$2
prefix=$3
final_plan_config_file=$4
plan_count=$5

shift 5

for ((i = 1; i <= $plan_count; i++)); 
do
    plan_name="$prefix-$i"
    [ $plan_count -eq 1 ] && plan_name="$prefix"
    plan_file_name="$plan_name.json"

    echo "Creating plan with name plan_name: $plan_name"
    coreo plan add --composite-id "$composite_id" --cloud-id "$cloud_id" --name "$plan_name" "$@"


    plan_id=`jq -r .planId "$plan_file_name"`
    plan_config_id=`jq -r .id "$plan_file_name"`

    echo "Finalizing plan with config_file: $final_plan_config_file, plan_id: $plan_id and plan_config_id: $plan_config_id "
    coreo plan finalize -f "$final_plan_config_file" --plan-id "$plan_id" --planconfig-id "$plan_config_id" "$@"

    rm -f "$plan_file_name"
done

