#!/bin/sh
# Requires jq and coreo cli
# Usage to create duplicate plans with all cloud accounts
# sh create_plan_per_cloud_account.sh COREO_COMPOSITE_ID PLAN_CONFIG_SOURCE_JSON [additional coreo CLI options]

[ $# -ge 2 ] || exit 1

composite_id=$1
final_plan_config_file=$2

shift 2

coreo cloud list --json "$@" | jq -r '.[] | .id + " " + .name' | while read cid name
do
    echo "Creating plan with name $name in cloud account $name ($cid)"
    sh create_duplicate_plans.sh "$composite_id" "$cid" "$name" "$final_plan_config_file" 1 "$@"
done

